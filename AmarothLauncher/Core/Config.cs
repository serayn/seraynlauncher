﻿
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;

namespace AmarothLauncher.Core
{
    /// <summary>
    /// Singleton responsible for getting config settings or generating default ones.
    /// </summary>
    public class Config
    {
        private static Config instance;

        // Launcher's version.
        public double version = 1.6; 
        public bool isDefaultConfigUsed { get; private set; }
        XmlDocument xml = new XmlDocument();
        XmlDocument defaultXml = new XmlDocument();
        OutputWriter o = OutputWriter.Instance;

        private Config()
        {
            Initialize();
        }

        public static Config Instance
        {
            get
            {
                if (instance == null)
                    instance = new Config();
                return instance;
            }
        }
        /// <summary>
        /// Generates object XML structure of 登录器配置文件.xml.
        /// If reading of XML failes, default settings are used.
        /// </summary>
        private void Initialize()
        {
            isDefaultConfigUsed = false;
            GenerateDefault();
            if (!File.Exists("登录器配置文件.xml"))
                UseDefault();
            else
            {
                StreamReader sr = new StreamReader("登录器配置文件.xml");
                string xmlString = sr.ReadToEnd();
                sr.Close();
                xml.LoadXml(xmlString);
            }
        }

        /// <summary>
        /// Use default config XML as current config XML.
        /// </summary>
        private void UseDefault()
        {
            xml = defaultXml;
            //o.Messagebox(SubElText("Messages", "XmlNotOpened"));

            // Save default config as a new config XML. Use for generating XML to be able to edit it afterwards, do NOT have this uncommented in releases!
            // SaveDefault();

            isDefaultConfigUsed = true;
        }

        /// <summary>
        /// Outputs whole config content. For debugging only.
        /// </summary>
        public void OutputContent()
        {
            o.Output("Outputing all values set in Config for debugging purpouses. * marks attributes (followed by their names), \"\" mark values.");
            foreach (XmlNode node in xml.DocumentElement.ChildNodes)
            {
                o.Output(node.Name, true);
                foreach (XmlNode att in node.ChildNodes)
                    o.Output("* " + att.Name + " - \"" + att.InnerText + "\"");
            }
        }

        /// <summary>
        /// Returns an inner text of given subelement of given element. If anything isn't found, error message is shown and an empty string returned.
        /// </summary>
        public string SubElText(string elementName, string subElementName)
        {
            if (xml.GetElementsByTagName(elementName).Count > 0)
            {
                foreach (XmlNode node in xml.GetElementsByTagName(elementName)[0].ChildNodes)
                    if (node.Name == subElementName)
                        return node.InnerText;
            }
            else
                o.Output(elementName + " 元素未能在配置文件中找到。这可能导致致命错误。");

            o.Output(subElementName + " 属性未能在配置文件中找到。这可能导致致命错误。");
            return "";
        }

        /// <summary>
        /// Return an inner text of given element in config XML. If not found, return empty string and output error.
        /// </summary>
        public string InnText(string elementName)
        {
            if (xml.GetElementsByTagName(elementName).Count > 0)
                return xml.GetElementsByTagName(elementName)[0].InnerText;
            else
            {
                o.Output(elementName + " 元素未能在配置文件中找到。这可能导致致命错误。");
                return "";
            }
        }

        #region Default config generation...
        /// <summary>
        /// Generate a new default config. Will be used only if no config is found.
        /// </summary>
        private void GenerateDefault()
        {
            XmlDeclaration declaration = defaultXml.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = defaultXml.DocumentElement;
            defaultXml.InsertBefore(declaration, root);
            XmlElement configs = defaultXml.CreateElement("Config");
            XmlComment comment = defaultXml.CreateComment("This is a config file for Launcher. Feel free to edit whatever you want or even translate Launcher to your native language. Pay close attention to comments and documentation.");
            defaultXml.AppendChild(comment);
            defaultXml.AppendChild(configs);

            MainSettingsDefault();
            PathConfigDefault();
            MainWindowDefault();
            ChangelogEditorDefault();
            ChangelogBrowserDefault();
            FTPLoginWindowDefault();
            MessagesDefault();
        }

        /// <summary>
        /// Adds a new subnode under a node, with given name and inner text.
        /// </summary>
        private void AddSubnodeDefault(XmlNode node, string name, string value)
        {
            AddSubnodeDefault(node, name, value, "");
        }

        /// <summary>
        /// Adds a new subnode under a node, with given name and inner text. Also creates a given comment, if it isn't empty.
        /// </summary>
        private void AddSubnodeDefault(XmlNode node, string name, string value, string comment)
        {
            XmlNode newNode = defaultXml.CreateElement(name);
            if (comment != "" && comment != null)
            {
                XmlComment newComment = defaultXml.CreateComment(name + ": " + comment);
                node.AppendChild(newComment);
            }
            newNode.InnerText = value;
            node.AppendChild(newNode);
        }

        /// <summary>
        /// Default configuration of paths to web.
        /// </summary>
        private void MainSettingsDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Main settings of an application.");
            XmlNode node = defaultXml.CreateElement("Main");

            AddSubnodeDefault(node, "DeleteCache", "1", "1 or 0. If 1, Cache folder is always deleted.");
            AddSubnodeDefault(node, "KeepBackups", "1", "1 or 0. If 1, .ext_ files are being kept as backups. Recommended 1.");
            AddSubnodeDefault(node, "KeepBlizzlikeMPQs", "1", "1 or 0. If 1, blizzlike MPQs in Data are ignored by Launcher. Otherwise they are handled in a same manner as custom one. Recommended 1.");
            AddSubnodeDefault(node, "ForcedRealmlist", "1", "1 or 0. If 1, realmlist.wtf will always be updated to match realmlist.wtf in FilesRootPath.");
            AddSubnodeDefault(node, "FileProcessingOutputs", "1", "1 or 0. If 1, messages about downloading and unziping files will be shown.");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Default configuration of paths to web.
        /// </summary>
        private void PathConfigDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Paths to files and folders Launcher will work with. They are commonly all !CASE SENSITIVE!");
            XmlNode node = defaultXml.CreateElement("Paths");

            AddSubnodeDefault(node, "FilelistPath", "https://atheroz-1252054826.cos.ap-shanghai.myqcloud.com/filelist.conf", "Path to text filelist.");
            AddSubnodeDefault(node, "VersionPath", "http://www.atheroz.cn:12340/files/launcherversion.conf", "Path to text file which contains your Lancher's current version (version is a double value with . as separator!");
            AddSubnodeDefault(node, "LauncherPath", "http://www.atheroz.cn:12340/files/Launcher.zip", "Path to a zip file with Launcher files - used if Launcher finds itself outdated.");
            AddSubnodeDefault(node, "FilesRootPath", "https://atheroz-1252054826.cos.ap-shanghai.myqcloud.com/", "Path to folder with files. Paths in filelist are relative to this path.");
            AddSubnodeDefault(node, "ChangelogPath", "http://www.atheroz.cn:12340/files/changelog.xml", "!HTTP! path to changelog XML file.");
            AddSubnodeDefault(node, "ChangelogFTPPath", "ftp://115.159.156.195/", "!Full! !FTP! path to folder in which changelog is. Notice that //www/ part. You may want to use an IP instead of a domain name.");
            AddSubnodeDefault(node, "Webpage", "http://www.atheroz.cn:12340", "URL which is to be opened when user clicks on Project webpage button.");
            AddSubnodeDefault(node, "Registration", "http://atheroz.cn:12340/member.php?mod=register", "URL which is to be opened when user clicks on Registration button.");
            AddSubnodeDefault(node, "Instructions", "http://www.atheroz.cn:12340/files/launchermanual/", "URL which is to be opened when user clicks on Launcher manual button.");
            AddSubnodeDefault(node, "HelloImage", "http://www.atheroz.cn:12340/files/hello.jpg", "URL to image which is to be displayed in Main window (latest news image). Clicking on it opens a changelog browser.");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Default configuration of main window.
        /// </summary>
        private void MainWindowDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Visual settings for Main Window.");
            XmlNode node = defaultXml.CreateElement("MainWindow");

            AddSubnodeDefault(node, "WindowName", "艾萨洛斯登录器 " + version.ToString("F", CultureInfo.InvariantCulture), "Name of main window. Change this to match your project's name.");
            AddSubnodeDefault(node, "OutputBox", "工作信息：");
            AddSubnodeDefault(node, "OptionalBox", "手动更新文件:");
            AddSubnodeDefault(node, "CheckForUpdatesButton", "检查更新");
            AddSubnodeDefault(node, "UpdateButton", "更新");
            AddSubnodeDefault(node, "WebpageButton", "社区网站");
            AddSubnodeDefault(node, "RegistrationButton", "注册页面");
            AddSubnodeDefault(node, "LauncherInstructionsButton", "Launcher manual");
            AddSubnodeDefault(node, "DeleteBackupsButton", "删除过时补丁");
            AddSubnodeDefault(node, "ChangelogEditorButton", "Changelog editor");
            AddSubnodeDefault(node, "ChangelogBrowserButton", "阅览修改记录");
            AddSubnodeDefault(node, "LaunchButton", "执行");
            AddSubnodeDefault(node, "ProgressText", "下载中：");
            AddSubnodeDefault(node, "ProgressSeparator", " / ");
            AddSubnodeDefault(node, "DownloadSpeedUnits", "/秒, ");
            AddSubnodeDefault(node, "remaining", "剩余");
            AddSubnodeDefault(node, "downloaded", "已下载, ");
            AddSubnodeDefault(node, "ToolTipTotalSize", "总大小: ");
            AddSubnodeDefault(node, "PanelTotalSize", "需更新总大小:");
            AddSubnodeDefault(node, "LabelTotalSizeOpt", "已选择手动更新内容: ");
            AddSubnodeDefault(node, "LabelTotalSizeNonOpt", "自动更新内容: ");
            AddSubnodeDefault(node, "second", " 秒 ");
            AddSubnodeDefault(node, "minute", " 分 ");
            AddSubnodeDefault(node, "hour", " 时 ");
            AddSubnodeDefault(node, "Complete", "下载完成！");
            AddSubnodeDefault(node, "Errors", "发生错误！");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Default configuration of changelog editor.
        /// </summary>
        private void ChangelogEditorDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Visual settings for Changelog Editor. A lot of those are used by Changelog Browser as well.");
            XmlNode node = defaultXml.CreateElement("ChangelogEditor");

            AddSubnodeDefault(node, "WindowName", "Changelog Editor");
            AddSubnodeDefault(node, "ChangelogEntries", "Changelog entries:");
            AddSubnodeDefault(node, "DateColumn", "日期");
            AddSubnodeDefault(node, "HeadingColumn", "标题");
            AddSubnodeDefault(node, "Date", "日期:");
            AddSubnodeDefault(node, "DateFormat", "dd.MM.yyyy hh:mm", "Carefully with this. MM for months, mm for minutes. You can use your own format, but it must be correct. Changelog's data must also be compatible with this, if your changelog isn't empty when this is being changed!");
            AddSubnodeDefault(node, "PictureURL", "Picture URL:");
            AddSubnodeDefault(node, "Heading", "Heading:");
            AddSubnodeDefault(node, "PicturePreview", "Picture preview:");
            AddSubnodeDefault(node, "Description", "Description:");
            AddSubnodeDefault(node, "EditEntryButton", "Edit entry");
            AddSubnodeDefault(node, "DeleteEntryButton", "Delete entry");
            AddSubnodeDefault(node, "CreateEntryButton", "Create entry");
            AddSubnodeDefault(node, "SaveEntryButton", "Save entry");
            AddSubnodeDefault(node, "TestPictureButton", "Test picture");
            AddSubnodeDefault(node, "CancelButton", "Cancel changes");
            AddSubnodeDefault(node, "SaveButton", "Save changelog");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Default configuration of changelog browser. A lot of settings are being used from changelog editor.
        /// </summary>
        private void ChangelogBrowserDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Visual settings for Changelog Browser.");
            XmlNode node = defaultXml.CreateElement("ChangelogBrowser");

            AddSubnodeDefault(node, "WindowName", "阅览修改记录");
            AddSubnodeDefault(node, "InfoText", "在项目列表点击项目以显示其内容。");
            AddSubnodeDefault(node, "Picture", "图片:");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Default configuration of FTP login window.
        /// </summary>
        private void FTPLoginWindowDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Visual settings for authentization dialog window for Changelog Editor.");
            XmlNode node = defaultXml.CreateElement("FTPLoginWindow");

            AddSubnodeDefault(node, "WindowName", "Login to FTP");
            AddSubnodeDefault(node, "Login", "Login:");
            AddSubnodeDefault(node, "Password", "Password:");
            AddSubnodeDefault(node, "OKButton", "OK");
            AddSubnodeDefault(node, "CancelButton", "Cancel");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Error (and other) messages. Leave space behind message if you want to ouput something directly behind it (like file's name).
        /// </summary>
        private void MessagesDefault()
        {
            XmlComment comment = defaultXml.CreateComment("Various messages which can be output by Launcher.");
            XmlNode node = defaultXml.CreateElement("Messages");

            AddSubnodeDefault(node, "HelloMessage", "这个登录器由www.model-changing.net的Amaroth进行开发，由艾萨洛斯开发组进行优化。", "Please, leave this here. If you want to add anything, add it behind original message.");
            AddSubnodeDefault(node, "XmlNotOpened", "初次使用，即将使用内置信息进行更新。请在更新完成后再再次打开本登录器以确保一切内容最新。");
            AddSubnodeDefault(node, "ChangelogNotOpened", "无法打开网站上的更新记录文件。");
            AddSubnodeDefault(node, "ChangelogNotLoaded", "无法打开更新记录数据。请联系艾萨洛斯开发组。");
            AddSubnodeDefault(node, "ChangelogEmpty", "Warning: changelog is empty. You are currently creating a new one.");
            AddSubnodeDefault(node, "InvalidFtpPassword", "Incorrect login - password combination, or incorrect FTP path to changelog.");
            AddSubnodeDefault(node, "PictureNotOpened", "Picture from given URL could not be opened. URL seems to be invalid.");
            AddSubnodeDefault(node, "ChangelogNotUploaded", "Changelog could not be uploaded. Backup XML file can be found in Launcher's directory.");
            AddSubnodeDefault(node, "ChangelogUploadOK", "Changelog was succesfully updated.");
            AddSubnodeDefault(node, "UnZipingFileError", "解压缩文件失败：");
            AddSubnodeDefault(node, "DownloadingFrom", "下载文件于: ");
            AddSubnodeDefault(node, "DownloadingTo", "到: ");
            AddSubnodeDefault(node, "UnzipingFile", "正在解压缩文件：");
            AddSubnodeDefault(node, "FileDeletingError", "无法删除文件：");
            AddSubnodeDefault(node, "WowExeMissing", "Game.exe未能找到！");
            AddSubnodeDefault(node, "DataDirMissing", "无法找到数据文件夹！");
            AddSubnodeDefault(node, "BlizzlikeMpqMissing", "无法找到以下文件：");
            AddSubnodeDefault(node, "LauncherNotInWowDir", "你的艾萨洛斯客户端已经受损，或者登录器没有放在艾萨洛斯文件夹内。");
            AddSubnodeDefault(node, "FilelistOpeningFailed", "登录器无法打开网站上的文件列表。请检查你的互联网连接或者联系艾萨洛斯开发组。错误信息：");
            AddSubnodeDefault(node, "FilelistReadingFailed", "网站上的文件列表无效，请联系艾萨洛斯开发组。");
            AddSubnodeDefault(node, "FileOnsWebMissing", "无法计算文件大小。网站上的文件可能已经丢失。");
            AddSubnodeDefault(node, "WebRealmlistMissing", "网站上无法找到realmlist.wtf。服务器列表无法验证。");
            AddSubnodeDefault(node, "RealmlistMissing", "realmlist.wtf丢失。即将建立一个新的代替。");
            AddSubnodeDefault(node, "OptionalsPresetLoadFailed", "你没有保存选项列表的选项，或者列表已经改变。请在进行更新前注意选项文件列表内容。");
            AddSubnodeDefault(node, "DownloadError", "下载以下文件时发生错误：");
            AddSubnodeDefault(node, "FileDownloadError", "部分文件未能完成下载。请重新运行更新检查以及更新。");
            AddSubnodeDefault(node, "HelloImageNotLoaded", "新图像无法载入。");
            AddSubnodeDefault(node, "VersionNotVerified", "登录器无法验证是否已经最新。它会正常工作，但如果问题持续出现，请通知艾萨洛斯开发团队。");
            AddSubnodeDefault(node, "CouldNotBeUpdated", "登录器尝试自我更新，但未成功。请重新运行登录器，如果问题持续出现请联系艾萨洛斯开发组。");
            AddSubnodeDefault(node, "OutdatedLauncher", "网站上检测到有登录器的更新版本。登录器尝试更新并重启。");
            AddSubnodeDefault(node, "LauncherUpdated", "登录器已成功更新，请重新运行登录器。");
            AddSubnodeDefault(node, "Removing", "正在删除文件: ");

            defaultXml.DocumentElement.AppendChild(comment);
            defaultXml.DocumentElement.AppendChild(node);
        }

        /// <summary>
        /// Save default XML as new 登录器配置文件.xml, overwrite an old one.
        /// </summary>
        private void SaveDefault()
        {
            TextWriter tw = new StreamWriter("登录器配置文件.xml", false, Encoding.UTF8);
            defaultXml.Save(tw);
            tw.Close();
        }
        #endregion
    }
}